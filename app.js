$(document).ready(function () {
    loadData();

    $('.btn-add').click(function(e) {
        const type = $(this).data('task');
        $('#type').val(type);
        $('.modal-title').text('Add ' + type);
        $("#modal").modal("show");
    });

    $("#btnSave").click(function(e) {
        const id = $('#id').val();
        const type = $('#type').val();
        const title = $('#title').val();
        const description = $('#description').val();
        if (id != '') {
            let tasks = JSON.parse(localStorage.getItem(type)) || [];
            tasks = tasks.map(task => {
                if (task.id == id) {
                    task.title = title;
                    task.description = description;
                }
                return task;
            });
            localStorage.setItem(type, JSON.stringify(tasks));
            $("#modal").modal("hide");
            $("#title").val("");
            $("#description").val("");
            $("#id").val("");
            loadData();
            return;
        }

        // save to state and local storage
        const task = {
            id: new Date().getTime(),
            title: title,
            description: description,
        };

        let tasks = JSON.parse(localStorage.getItem(type)) || [];
        tasks.push(task);
        localStorage
            .setItem(type, JSON.stringify(tasks));
        $("#modal").modal("hide");
        $("#title").val("");
        $("#description").val("");
        $("#id").val("");
        loadData();
    });

    function addListener() {
        $('.btn-delete').click(function(e) {
            Swal.fire({
                title: "Are you sure?",
                text: "You won't be able to revert this!",
                icon: "warning",
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                confirmButtonText: "Yes, delete it!"
              }).then((result) => {
                if (result.isConfirmed) {
                    const id = $(this).data('id');
                    const type = $(this).data('type');
                    let tasks = JSON.parse(localStorage.getItem(type)) || [];
                    tasks = tasks.filter(task => task.id !== id);
                    localStorage.setItem(type, JSON.stringify(tasks));
                    loadData();
                    Swal.fire({
                        title: "Deleted!",
                        text: "Your task has been deleted.",
                        icon: "success"
                    });
                }
              });
        });

        $('.btn-edit').click(function(e) {
            const id = $(this).data('id');
            const type = $(this).data('type');
            const tasks = JSON.parse(localStorage.getItem(type)) || [];
            const task = tasks.find(task => task.id === id);
            $('#id').val(task.id);
            $('#type').val(type);
            $('#title').val(task.title);
            $('#description').val(task.description);
            $('.modal-title').text('Edit ' + type);
            $("#modal").modal("show");
        });
    }

    function loadData() {
        const types = ['todo', 'doing', 'hold', 'resources'];
        types.forEach(type => {
            $(`#board-${type}`).empty();
            let tasks = JSON.parse(localStorage.getItem(type)) || [];
            tasks.forEach(task => {
                const html = `
                    <div class="card mt-2">
                        <div class="card-body">
                            <h5 class="card-title
                            ">${task.title}</h5>
                            <p class="card-text">${task.description}</p>
                            <button class="btn btn-danger btn-delete" data-id="${task.id}" data-type="${type}">Delete</button>
                            <button class="btn btn-success btn-edit" data-id="${task.id}" data-type="${type}">Edit</button>
                        </div>
                    </div>
                `;
                $(`#board-${type}`).append(html);
            });
        });
        addListener();
    }
});